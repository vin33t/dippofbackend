<?php

use App\Http\Controllers\DoctorController;
use App\Http\Controllers\HomeController;
use App\Models\Categories;
use App\Models\Doctors;
use App\Models\Listing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\ListingController;
use App\Http\Controllers\API\ResponseController as ResponseController;
use App\Http\Controllers\GeneralController;
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    $user = [];
    $user['name'] = $request->user()->name;
    $user['phone'] = $request->user()->phone;
    $user['email'] = $request->user()->email;
    $user['profileImage'] = $request->user()->getMedia()->count() ? $request->user()->getFirstMediaUrl() : null;
    $user['referralCode'] = $request->user()->referralCode;
    $user['referredBy'] = null;
//    $res = [
//        'success' => true,
//        'data' => $user,
//        'message' => 'User Details',
//    ];
    return response()->json($user, 200);
});

Route::middleware('auth:sanctum')->post('/user/update', function (Request $request) {
    $user = $request->user();
    $request->validate([
        'name' => 'string',
        'phone' => 'digits:10',
        'profileImage' => 'image|mimes:jpg,png,jpeg,gif,svg|max:5062',
        'email' => 'email|unique:users,email'
    ], [
        'name.required' => 'name required',
        'phone.digits' => 'phone should be of 10 digits'
    ]);
//    (String)\Ramsey\Uuid\Uuid::uuid1();

    $request->name ? $user->update(['name' => $request->name]) : null;
    $request->phone ? $user->update(['phone' => $request->phone]) : null;
    $request->email ? $user->update(['email' => $request->email]) : null;
    if($request->profileImage){
        if($user->getMedia()->count()){
            $user->getFirstMedia()->delete();
        }
        $user->addMedia($request->profileImage)->toMediaCollection();
    }
    $res = [
        'success' => true,
        'data' => [],
        'message' => 'User Details Updated',
    ];
    return response()->json($res, 200);


});


Route::post('sendOtp', [AuthController::class, 'sendOtp']);
Route::post('verifyOtp', [AuthController::class, 'verifyOtp']);

Route::get('categories', [ListingController::class, 'viewCategories']);


Route::post('listing', function (Request $request) {
    if ($request->type && $request->type == 'doctor') {
        $doctorListing = Doctors::where('approved', '!=', 0);
        if ($request->filter) {
            $listings = $doctorListing->withAllTags($request->filter)->get();

            $allFilters = collect();
            $allFilters->push('Broiler Consultant');
            $allFilters->push('Breeder Consultant');
            $allFilters->push('Layer Consultant');
            $allFilters->push('Feed Consultant');

//            $allFilters = (new App\Http\Controllers\ListingController)->getCategoryFilters($request->categoryId);
            $activeFilters = [];
            foreach ($allFilters as $filter) {
                $activeFilters[$filter] = in_array($filter, $request->filter) ? true : false;
            }
        } else {
            $listings = $doctorListing->get();
        }


        $doctors = $listings->map(function ($doctor) {
            $images = collect();
            foreach ($doctor->getMedia() as $media) {
                $images->push($media->getFullUrl());
            }
            $doctor->images = $images;
            return $doctor;
        });

        $res = [
            'success' => true,
            'data' => $doctors,
            'message' => $listings->count() ? 'Doctors Listing Fetched' : 'No Listings found for this category',
        ];
        $request->filter ? $res['filters'] = $activeFilters : null;
        return response()->json($res, 200);
    } else {
        $request->validate([
            'categoryId' => 'integer',
            'filter.*' => 'string'
        ]);
        if(isset($request->categoryId)){
            $categoryListing = Listing::where('categoryId', $request->categoryId)->orderBy('created_at','DESC');
        } else {
            $categoryListing = Listing::where('categoryId','!=', 0)->orderBy('created_at','DESC');
        }
//        return ($categoryListing->first()['extra_attributes']['lat']);
        if ($request->filter) {
            $listings = $categoryListing->withAllTags($request->filter)->orderBy('created_at','DESC')->get();
            $allFilters = (new App\Http\Controllers\ListingController)->getCategoryFilters($request->categoryId);
            $activeFilters = [];
            foreach ($allFilters as $filter) {
                $activeFilters[$filter] = in_array($filter, $request->filter) ? true : false;
            }
        } else {
            $listings = $categoryListing->orderBy('created_at','DESC')->get();
        }

        $result1 = $listings->map(function ($listing) use ($request) {
            $images = collect();
            foreach ($listing->getMedia() as $media) {
                $images->push($media->getFullUrl());
            }
            $images->count() ? $listing->images = $images : $listing->images = null;
            $listing->extra_attributes->count() ? $listing->has_extra_attributes = true : $listing->has_extra_attributes = false;

            $listing->extra_attributes = [$listing->extra_attributes->count() ? $listing->extra_attributes : null];
            $listing->description = $listing->description;
            $listing->average_rating = $listing->averageRating();
//            if(isset($request->lat) && isset($request->long)){
//                $listing->extra_attributes->long;
//                return [$request->lat, $request->long, $listing->extra_attributes->lat, $listing->extra_attributes->long];
//                return distance($request->lat, $request->long, $listing->extra_attributes->lat, $listing->extra_attributes->long);
//                if(distance($request->lat, $request->long, $listing->extra_attributes->lat, $listing->extra_attributes->long) < 10){
//                    return $listing;
//                }
//            }else {
            return $listing;
//            }
        });


        $res = [
            'success' => true,
            'data' => $result1,
            'message' => $listings->count() ? 'Listing Fetched' : 'No Listings found for this category',
        ];
        $request->filter ? $res['filters'] = $activeFilters : null;
        return response()->json($res, 200);
    }
});


Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::get('mylistings', [ListingController::class, 'myListings']);
    Route::get('myDoctorlistings', [ListingController::class, 'myDoctorListings']);
    Route::post('editListing', [ListingController::class, 'editListing']);
    Route::get('myJobs', [ListingController::class, 'myJobs']);
    Route::get('banners', [ListingController::class, 'bannerList']);
    Route::post('doctor/list', [DoctorController::class, 'DoctorsListing']);
//    Route::post('/createListing', [ListingController::class, 'createListing']);
    Route::post('/createListing', [ListingController::class, 'createListing']);
    Route::post('/reviewAndRateListing', [ListingController::class, 'reviewAndRateListing']);
    Route::post('/getListingReviewsAndRatings', [ListingController::class, 'listingReviewsAndRatings']);
    Route::post('/createCategory', [ListingController::class, 'createCategory']);
    Route::post('/doctor/create', [DoctorController::class, 'CreateDoctor']);
    Route::post('/doctor/edit', [DoctorController::class, 'EditDoctor']);
    Route::get('/doctor/filters', function () {
        $filters = collect();
        $filters->push('Broiler Consultant');
        $filters->push('Breeder Consultant');
        $filters->push('Layer Consultant');
        $filters->push('Feed Consultant');
        $res = [
            'success' => true,
            'data' => $filters,
            'message' => 'Doctor Filters',
        ];

        return response()->json($res, 200);
    });

    Route::get('/job/filters', function () {
        $filters = collect();
        $filters->push('Veterinary Doctor');
        $filters->push('Integration Company');
        $filters->push('Feed Mill');
        $filters->push('Breeder & Hatchery');

        $res = [
            'success' => true,
            'data' => $filters,
            'message' => 'Job Filters',
        ];

        return response()->json($res, 200);
    });
    Route::post('/applyjob', [\App\Http\Controllers\JobsController::class, 'applyJob']);


    Route::post('/filters', [ListingController::class, 'filters']);

    Route::post('/write-to-us', [GeneralController::class,'writeToUs']);
    Route::get('/write-to-us/list', [GeneralController::class,'writeToUsList']);
    Route::post('/write-to-us/review/list', [GeneralController::class,'commentReviewsAndRatings']);
    Route::post('/write-to-us/review', [GeneralController::class,'reviewAndRateComments']);
});



