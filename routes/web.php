<?php

use App\Http\Controllers\DoctorController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});


Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/categories', [HomeController::class, 'categories'])->name('categories');
Route::get('/listings', [HomeController::class, 'listings'])->name('listings');
Route::get('/doctors',[DoctorController::class,'index'])->name('doctors');
Route::post('/doctor/update/status/{doctor}',[DoctorController::class,'updateStatus'])->name('doctor.statusUpdate');
Route::get('/banners',[HomeController::class,'banners'])->name('banners');
Route::post('/banners/upload',[HomeController::class,'bannerUpload'])->name('banner.upload');
