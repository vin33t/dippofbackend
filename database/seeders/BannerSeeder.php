<?php

namespace Database\Seeders;

use App\Models\Banners;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $banners = [
            ['name' => 'Banner 1', 'redirectsTo' => 1, 'image' => 'https://essezymb.sirv.com/poultryMart/banners/1.jpg'],
            ['name' => 'Banner 1', 'redirectsTo' => 2, 'image' => 'https://essezymb.sirv.com/poultryMart/banners/2.jpg'],
            ['name' => 'Banner 1', 'redirectsTo' => 3, 'image' => 'https://essezymb.sirv.com/poultryMart/banners/3.jpg']
        ];
        foreach ($banners as $banner) {
            $newBanner = Banners::create([
                'name' => $banner['name'],
                'categoryId' => $banner['redirectsTo']
            ]);
            $newBanner->addMediaFromUrl($banner['image'])->toMediaCollection();
        }
    }
}
