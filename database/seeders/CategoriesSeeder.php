<?php

namespace Database\Seeders;

use App\Models\Categories;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            ['name'=>'Broiler','filters'=>["Integration Company",'Farmers'],'image'=>'https://essezymb.sirv.com/poultryMart/broiler.png'],
            ['name'=>'Chicks','filters'=>["Broiler Chicks","Layer Chicks"],'image'=>'https://essezymb.sirv.com/poultryMart/chicks.png'],
            ['name'=>'Hatching Eggs','filters'=>[],'image'=>'https://essezymb.sirv.com/poultryMart/hatching_eggs.png'],
            ['name'=>'Eggs','filters'=>[],'image'=>'https://essezymb.sirv.com/poultryMart/eggs.png'],
            ['name'=>'Poultry Equipments','filters'=>['EC Shed Equipment','Drinkers & Feeders','Process Equipment','Cages'],'image'=>'https://essezymb.sirv.com/poultryMart/poultry_equipments.png'],
            ['name'=>'Lab Services','filters'=>['Raw Material & Food Testing', 'Diseases Diagnosis'],'image'=>'https://essezymb.sirv.com/poultryMart/lab_service.png'],
            ['name'=>'Feed','filters'=>['Feed Manufacturer','Feed Dealer'],'image'=>'https://essezymb.sirv.com/poultryMart/poultry_feed.png'],
            ['name'=>'Raw Material','filters'=>['Soya','Maize','Bajra','MBM','Ddgs','Dorb','Oil','Mustard DOC','Broken Rice','Wheat','Rice Polish','Maize Gluten','DCP','MCP'],'image'=>'https://essezymb.sirv.com/poultryMart/raw_material.png'],
            ['name'=>'Poultry Medicine','filters'=>['Company','Distributor'],'image'=>'https://essezymb.sirv.com/poultryMart/poultry_medicine.png'],
        ];
        foreach($categories as $category){
            $newCategory = Categories::create([
                'name'=>$category['name'],
                'description'=>'na'
            ]);
            count($category['filters']) ? $newCategory->attachTags($category['filters']): null;
            $newCategory->addMediaFromUrl($category['image'])->toMediaCollection();

        }
    }
}
