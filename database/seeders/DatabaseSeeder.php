<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//         \App\Models\User::factory(10)->create();
        $this->call(CategoriesSeeder::class);
        $this->call(BannerSeeder::class);

        User::create([
            'name' => 'Admin',
            'email' => 'admin@solutions1313.com',
            'password' => Hash::make('password1313'),
            'phone' => '9876543210'
        ]);
    }
}
