<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('whatsapp_no');
            $table->string('location');
            $table->string('degree');
            $table->string('experienceMonths');
            $table->string('experienceYears');
            $table->string('specialization');
            $table->integer('fees');
            $table->time('availableTimeFrom');
            $table->time('availableTimeTo');
            $table->string('description');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctors');
    }
};
