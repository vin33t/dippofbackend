<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('management_likeand_comments', function (Blueprint $table) {
            $table->id();
            $table->integer('managementCommentId');
            $table->integer('userId');
            $table->string('type')->comment('like, comment');
            $table->integer('parentId')->unsigned()->nullable();
            $table->longText('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('management_likeand_comments');
    }
};
