<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function convertValidationExceptionToResponse(ValidationException $e, $request) {
        if($e instanceof ValidationException && $request->expectsJson()){
            return response()->json([
                "status" => 0,
                "statusCode" => 422,
                "message" =>  "The given data was invalid.",
                "result" => $e->errors()
            ]);
        }else{
            parent::convertValidationExceptionToResponse($e, $request);
        }

    }
}
