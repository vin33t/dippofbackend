<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\Tags\HasTags;

class Categories extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia,HasTags;

    protected $guarded = ['id'];

    public function listings()
    {
        return $this->hasMany('App\Models\Listing', 'categoryId');
    }
}
