<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Codebyray\ReviewRateable\Traits\ReviewRateable as ReviewRateableTrait;

class ManagementComments extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia, ReviewRateableTrait;
    protected $guarded=['id'];

    public function likeOrComment(){
        return $this->hasMany('App\Models\ManagementLikeandComments','managementCommentId')->whereNull('parentId');
    }
}
