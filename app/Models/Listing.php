<?php

namespace App\Models;

use Codebyray\ReviewRateable\Contracts\ReviewRateable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\SchemalessAttributes\Casts\SchemalessAttributes;
use Spatie\Tags\HasTags;
use Codebyray\ReviewRateable\Traits\ReviewRateable as ReviewRateableTrait;
class Listing extends Model implements HasMedia, ReviewRateable
{
    use HasFactory, InteractsWithMedia, HasTags, ReviewRateableTrait;

    public $casts = [
        'extra_attributes' => SchemalessAttributes::class,
    ];

    public function scopeWithExtraAttributes(): Builder
    {
        return $this->extra_attributes->modelScope();
    }

    protected $guarded = ['id'];

    public function category()
    {
        return $this->belongsTo('App\Models\Categories', 'categoryId', 'id');
    }
}
