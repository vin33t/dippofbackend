<?php

namespace App\Http\Controllers;

use App\Http\Controllers\API\ResponseController;
use App\Models\ManagementComments;
use App\Models\ManagementLikeandComments;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GeneralController extends ResponseController
{
    public function writeToUs(Request $request)
    {
        $request->validate([
            'type' => 'required|string',
            'category' => 'required|string',
            'remark' => 'required|string',
            'media' => 'required|file|mimes:jpg,png,jpeg,gif,svg,mp4,mov,ogg,qt|max:20480'
        ],
            [
                'type.required' => 'type is required',
                'category.required' => 'category is required',
                'remark.required' => 'remark is required',
                'media.required' => 'media is required',
            ]);

        $comment = ManagementComments::create([
            'userId' => $request->user()->id,
            'type' => $request->type,
            'category' => $request->category,
            'remark' => $request->remark,
        ]);
        $comment->addMedia($request->media)->toMediaCollection();

        return $this->handleResponse(null, 'Message Sent Successfully');

    }

    public function writeToUsList()
    {
        $allComments = ManagementComments::all();

//        return $allComments;
        $mComments = collect();
        if ($allComments->count()) {
            foreach ($allComments as $aMComments) {
                $mComments->push(
                    [
                        'id' => $aMComments->id,
                        'commentBy' => User::find($aMComments->userId)->name,
                        'category' => $aMComments->category,
                        'remark' => $aMComments->remark,
                        'likes' => ManagementLikeandComments::where('managementCommentId', $aMComments->id)->where('type', 'like')->count(),
                        'unlikes' => ManagementLikeandComments::where('managementCommentId', $aMComments->id)->where('type', 'unlike')->count(),
                        'comments' => ManagementLikeandComments::where('managementCommentId', $aMComments->id)->where('type', 'like')->count(),
                        'file'=>$aMComments->getFirstMediaUrl(),
                        'isLiked'=>$aMComments->userId == auth()->user()->id ? true : false,
                        'created_at' => $aMComments->created_at
                    ]);
            }
        } else {
            return $this->handleResponse(null, 'No Likes and Comments found');
        }

        return $this->handleResponse($mComments, 'Rated and Reviewed Successfully');

    }

    public function reviewAndRateComments(Request $request)
    {
        $request->validate([
            'commentId' => 'required|integer',
            'comment' => 'required_without:like|string',
            'like' => 'required_without:comment|boolean',
            'parentId' => 'integer:management_likeand_comments,id',
        ], [
            'commentId.required' => 'commentId is required',
            'comment.required' => 'comment is required',
            'like.required' => 'like is required',
        ]);
        $comment = ManagementComments::findOrFail($request->commentId);

        if (isset($request->parentId)) {
            $comment->likeOrComment()->create([
                'userId' => auth()->user()->id,
                'parentId' => $request->parentId,
                'type' => isset($request->like) ? ($request->like == 1 ? 'like' : 'unlike') : 'comment',
                'comment' => isset($request->comment) ? $request->comment : null,
            ]);
        } else {
            $comment->likeOrComment()->create([
                'userId' => auth()->user()->id,
                'type' => isset($request->like) ? ($request->like == 1 ? 'like' : 'unlike') : 'comment',
                'comment' => isset($request->comment) ? $request->comment : null,
            ]);
        }
//        $rating = $comment->rating([
//            'title' => $request->reviewTitle,
//            'body' => $request->reviewBody,
//            'rating' => $request->rating,
//            'recommend' => 'Yes',
//            'approved' => true, // This is optional and defaults to false
//        ], $request->user());

        return $this->handleResponse('Success', 'Rated and Reviewed Successfully');

//        return $request;
    }

    public function commentReviewsAndRatings(Request $request)
    {
//        $comment = ManagementComments::findOrFail($request->commentId);
//        return $comment;

        $likes = 0;
        $unlikes = 0;
        $comments = collect();
        if (isset($request->commentId)) {
            $ratings = ManagementLikeandComments::where('managementCommentId', $request->commentId)->get();
        } else {
            $ratings = ManagementLikeandComments::all();
        }
        if ($ratings->count()) {
            foreach ($ratings as $rating) {
                $rating->type == 'like' ? $likes++ : $comments->push(['comment' => $rating->comment, 'commentBy' => User::find($rating->userId)->name]);
                $rating->type == 'unlike' ? $unlikes++ : null;
//                $rating->type == 'comment' ? $likes++ : null;
            }
        } else {
            return $this->handleResponse(null, 'No Likes and Comments found');
        }

//        $ratings = $comment->getAllRatings($comment->id)->map(function ($rating) {
//            unset($rating['customer_service_rating']);
//            unset($rating['quality_rating']);
//            unset($rating['friendly_rating']);
//            unset($rating['pricing_rating']);
//            unset($rating['recommend']);
//            unset($rating['department']);
//            unset($rating['approved']);
//            unset($rating['reviewrateable_type']);
//            unset($rating['author_type']);
//            unset($rating['author_id']);
//            unset($rating['updated_at']);
//            unset($rating['reviewrateable_id']);
//            return $rating;
//        });

        return $this->handleResponse(['likes' => $likes,'unlikes' => $unlikes, 'comments' => $comments], 'Comments and Likes Fetched Successfully');

    }
}
