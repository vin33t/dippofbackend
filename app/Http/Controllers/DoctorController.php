<?php

namespace App\Http\Controllers;

use App\Models\Doctors;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\API\ResponseController as ResponseController;

class DoctorController extends ResponseController
{
    /**
     * Returning total doctos on Admin Dashboard.
     *
     * @return Application|Factory|View
     */


    public function index() {
        $doctors = Doctors::all();
        return view('doctors', compact('doctors'));
    }

    /**
     * Api Endpoint Controller for Doctor Registration
     *
     * @param Request $request
     * @return Application|ResponseFactory|JsonResponse|Response
     */
    public function CreateDoctor(Request $request) {
        $validator = Validator::make($request->all(), [
            'filters'=>'required',
            'filters.*'=>'required|string',
            'name' => 'required|string',
            'whatsappNumber' => 'required|numeric|digits:10',
            'location' => 'required|string',
            'degree' => 'required|string',
            'experienceMonths' => 'required|integer',
            'experienceYears' => 'required|integer',
            'specialization' => 'required|string',
            'fees' => 'required|integer',
            'availableTimeFrom' => 'required|date_format:H:i',
            'availableTimeTo' => 'required|date_format:H:i',
            'description' => 'required|string',
            'images.*'=>'required|image|mimes:jpg,png,jpeg,gif,svg',
        ],[
            'name.required' => 'name is required',
            'whatsappNumber.required' => 'whatsappNumber is required',
            'location.required' => 'location is required',
            'degree.required' => 'degree is required',
            'experienceMonths.required' => 'experienceMonths is required',
            'experienceYears.required' => 'experienceYears is required',
            'specialization.required' => 'specialization is required',
            'fees.required' => 'fee is required',
            'fees.integer' => 'fee must be a number',
            'availableTimeTo.required' => 'availableTimeTo is required',
            'availableTimeTo.date_format' => 'availableTimeTo format should be H:i',
            'availableTimeFrom.required' => 'availableTimeFrom is required',
            'availableTimeFrom.date_format' => 'availableTimeFrom format should be H:i',
            'description.required' => 'description is required',
            'images.*.required'=>'images are required',
        ]);
        if ($validator->fails()) {
            return response($validator->messages());
        }
        DB::beginTransaction();
        try {

            $doctor = Doctors::create([
                'userId'=>auth()->user()->id,
                'name'=>$request->name,
                'whatsapp_no'=>$request->whatsappNumber,
                'location'=>$request->location,
                'degree'=>$request->degree,
                'experienceMonths'=>$request->experienceMonths,
                'experienceYears'=>$request->experienceYears,
                'experience'=>$request->experience,
                'specialization'=>$request->specialization,
                'fees'=>$request->fees,
                'availableTimeFrom'=>$request->availableTimeFrom,
                'availableTimeTo'=>$request->availableTimeTo,
                'description'=>$request->description,
            ]);
            $doctor->attachTags($request->filters);
            foreach ($request->images as $image) {
                $doctor->addMedia($image)->toMediaCollection();
            }
            DB::commit();
            return $this->handleResponse($doctor, 'Doctor Created');
        } catch (\Exception $e){
            DB::rollBack();
            return $this->handleError($e, 'Unable to Add Record');

        }

    }

    public function EditDoctor(Request $request){
        $request->validate([
            'doctorListingId'=>'required|exists:doctors,id',
            'filters'=>'required',
            'filters.*'=>'required|string',
            'name' => 'required|string',
            'whatsappNumber' => 'required|numeric|digits:10',
            'location' => 'required|string',
            'degree' => 'required|string',
            'experience' => 'required|string',
            'specialization' => 'required|string',
            'fees' => 'required|integer',
            'availableTime' => 'required|date_format:H:i',
            'description' => 'required|string',
            'images.*'=>'required|image|mimes:jpg,png,jpeg,gif,svg',
        ],[
            'doctorListingId.required' => 'doctorListingIwd is required',
            'name.required' => 'name is required',
            'whatsappNumber.required' => 'whatsappNumber is required',
            'location.required' => 'location is required',
            'degree.required' => 'degree is required',
            'experience.required' => 'experience is required',
            'specialization.required' => 'specialization is required',
            'fees.required' => 'fee is required',
            'fees.integer' => 'fee must be a number',
            'availableTime.required' => 'availableTime is required',
            'availableTime.date_format' => 'availableTime format should be H:i',
            'description.required' => 'description is required',
            'images.*.required'=>'images are required',
        ]);

        DB::beginTransaction();
        try {

            $doctor = Doctors::find($request->doctorListingId)->update([
                'name'=>$request->name,
                'whatsapp_no'=>$request->whatsappNumber,
                'location'=>$request->location,
                'degree'=>$request->degree,
                'experience'=>$request->experience,
                'specialization'=>$request->specialization,
                'fees'=>$request->fees,
                'available_time'=>$request->availableTime,
                'description'=>$request->description,
            ]);
            $doctor->syncTags($request->filters);
            foreach ($request->images as $image) {
                $doctor->addMedia($image)->toMediaCollection();
            }
            DB::commit();
            return $this->handleResponse($doctor, 'Doctor Updated');
        } catch (\Exception $e){
            DB::rollBack();
            return $this->handleError($e, 'Unable to Updated Record');
        }
    }

    public function DoctorsListing(Request $request) {
        $doctorListing = Doctors::where('id','!=',0);
        if ($request->filter) {
            $listings = $doctorListing->withAllTags($request->filter)->get();

            $allFilters = collect();
            $allFilters->push('Broiler Consultant');
            $allFilters->push('Breeder Consultant');
            $allFilters->push('Layer Consultant');
            $allFilters->push('Feed Consultant');

//            $allFilters = (new App\Http\Controllers\ListingController)->getCategoryFilters($request->categoryId);
            $activeFilters = [];
            foreach($allFilters as $filter){
                $activeFilters[$filter] = in_array($filter, $request->filter) ?  true : false;
            }
        } else {
            $listings = $doctorListing->get();
        }


        $doctors = $listings->map(function($doctor){
            $images = collect();
            foreach($doctor->getMedia() as $media){
                $images->push($media->getFullUrl());
            }
            $doctor->images = $images;
            return $doctor;
        })->reverse();

        $res = [
            'success' => true,
            'data'    => $doctors,
            'message' => $listings->count() ? 'Listing Fetched' : 'No Listings found for this category',
        ];
        $request->filter ? $res['filters'] = $activeFilters : null;
        return response()->json($res, 200);

//        return $this->handleResponse($doctors, 'Doctors Fetched');
    }


    public function DoctorFilter(){
        $filters = collect();
        $filters->push('Broiler Consultant');
        $filters->push('Breeder Consultant');
        $filters->push('Layer Consultant');
        $filters->push('Feed Consultant');

        $res = [
            'success' => true,
            'data'    => $filters,
            'message' => 'Doctor Filters',
        ];

        return response()->json($res, 200);
    }

    public function updateStatus(Doctors $doctor, Request $request){
        if($request->approveDoctor == 'approve'){
            $doctor->update([
                'approved'=>1,
                'approval_at'=>Carbon::now()
            ]);
        }  elseif($request->approveDoctor == 'cancel'){
            $doctor->update([
                'approved'=>0,
                'approval_at'=>Carbon::now()
            ]);
        }
        return redirect()->back();
    }
}
