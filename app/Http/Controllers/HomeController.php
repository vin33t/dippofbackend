<?php

namespace App\Http\Controllers;

use App\Models\Banners;
use App\Models\Categories;
use App\Models\Listing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }


    public function categories()
    {
        $categories = Categories::all()->map(function ($category) {
            $images = collect();
            foreach ($category->getMedia() as $media) {
                $images->push($media->getFullUrl());
            }
            $category->images = $images;
            return $category;
        });
        return view('categories')->with('categories', $categories);
    }

    public function listings()
    {
        $listings = Listing::all()->map(function ($category) {
            $images = collect();
            foreach ($category->getMedia() as $media) {
                $images->push($media->getFullUrl());
            }
            $category->images = $images;
            return $category;
        });
        return view('listings')->with('listings', $listings);
    }

    public function banners()
    {
        $banners = Banners::all();
        return view('banners')->with(['banners' => $banners]);
    }


    public function bannerUpload(Request $request)
    {
        $request->validate([
            'bannerName' => 'required',
            'redirectsTo' => 'required|exists:categories,id',
            'bannerImage' => 'required|image|mimes:jpg,png,jpeg,gif,svg'
        ]);
        DB::beginTransaction();
        try{
            $banner = Banners::create([
                'name'=>$request->bannerName,
                'categoryId'=>$request->redirectsTo
            ]);
            $banner->addMedia($request->bannerImage)->toMediaCollection();
            DB::commit();
        }
        catch(\Exception $e){
            DB::rollBack();
        }

        return redirect()->back();
    }
}
