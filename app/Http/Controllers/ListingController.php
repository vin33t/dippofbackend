<?php

namespace App\Http\Controllers;

use App\Models\Banners;
use App\Models\Categories;
use App\Models\Doctors;
use App\Models\Jobs;
use App\Models\Listing;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\API\ResponseController as ResponseController;

class ListingController extends ResponseController
{
    public function myDoctorlistings(){
            $doctorListing = Doctors::where('userId', auth()->user()->id)->get();
        $res = [
            'success' => true,
            'data' => $doctorListing,
            'message' => $doctorListing->count() ? 'Listing Fetched' : 'No Listings found for this category',
        ];

        return response()->json($res, 200);
    }

    public function myListings()
    {
        $listings = Listing::where('userId', auth()->user()->id)->get();
        $result1 = $listings->map(function ($listing) {
            $images = collect();
            foreach ($listing->getMedia() as $media) {
                $images->push($media->getFullUrl());
            }
            $images->count() ? $listing->images = $images : $listing->images = null;
            $listing->extra_attributes->count() ? $listing->has_extra_attributes = true : $listing->has_extra_attributes = false;

            $listing->extra_attributes = [$listing->extra_attributes->count() ? $listing->extra_attributes : null];
            $listing->average_rating = $listing->averageRating();
            $listing->categoryName = $listing->category->name;
            return $listing;
        });


        $res = [
            'success' => true,
            'data' => $result1,
            'message' => $listings->count() ? 'Listing Fetched' : 'No Listings found for this category',
        ];

        return response()->json($res, 200);
    }

    public function myJobs()
    {
        $jobs = Jobs::where('userId', auth()->user()->id)->get();
        $result1 = $jobs->map(function ($listing) {
            $listing->cv = $listing->getFirstMediaUrl();
            return $listing;
        });

        $res = [
            'success' => true,
            'data' => $result1,
            'message' => $jobs->count() ? 'Jobs Fetched' : 'You haven\'t applied for a job yet',
        ];

        return response()->json($res, 200);
    }

    public function listingReviewsAndRatings(Request $request)
    {
        $listing = Listing::findOrFail($request->listingId);
        $ratings = $listing->getAllRatings($listing->id)->map(function ($rating) {
            unset($rating['customer_service_rating']);
            unset($rating['quality_rating']);
            unset($rating['friendly_rating']);
            unset($rating['pricing_rating']);
            unset($rating['recommend']);
            unset($rating['department']);
            unset($rating['approved']);
            unset($rating['reviewrateable_type']);
            unset($rating['author_type']);
            unset($rating['author_id']);
            unset($rating['updated_at']);
            unset($rating['reviewrateable_id']);
            return $rating;
        });
        return $this->handleResponse($ratings, 'Reviews Fetched Successfully');

    }

    public function reviewAndRateListing(Request $request)
    {
        $listing = Listing::findOrFail($request->listingId);
        $rating = $listing->rating([
            'title' => $request->reviewTitle,
            'body' => $request->reviewBody,
            'rating' => $request->rating,
            'recommend' => 'Yes',
            'approved' => true, // This is optional and defaults to false
        ], $request->user());

        return $this->handleResponse('Success', 'Listing Rated and Reviewed Successfully');

//        return $request;
    }

    public function bannerList()
    {
        $banners = collect();
        Banners::all()->map(function ($banner) use ($banners) {
            $b = [];
            $b['name'] = $banner->name;
            $b['redirectsTo'] = $banner->category->name;
            $b['image'] = $banner->getFirstMediaUrl();
            $banners->push($b);
        });
        return $this->handleResponse($banners, 'Banners Fetched');

    }

    public function viewListing(Request $request)
    {
        $request->validate([
            'categoryId' => 'required|integer'
        ], [
            'categoryId.required' => 'categoryId is required'
        ]);
        $category = Categories::find($request->categoryId);
        $listings = $category->listings->map(function ($listing) {
            $images = collect();
            foreach ($listing->getMedia() as $media) {
                $images->push($media->getFullUrl());
            }
            $listing->images = $images;
            return $listing;
        });
        return $this->handleResponse($listings, $listings->count() ? 'Listing Fetched' : 'No Listings found for this category');
    }

    public function viewCategories()
    {
        $categories = Categories::all()->map(function ($category) {
//            $images = collect();
//            foreach ($category->getMedia() as $media) {
//                $images->push($media->getFullUrl());
//            }
            $category->image = $category->getFirstMediaUrl();
            return $category;
        });
        return $this->handleResponse($categories, 'Categories Fetched');
    }

    public function createCategory(Request $request)
    {
//        return $request;
        $request->validate([
            'name' => 'required|string',
            'description' => 'required|string',
//            'images'=>'required',
            'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg',
        ]);
        $category = Categories::create($request->all());
        $category->addMedia($request->image)->toMediaCollection();
//        foreach ($request->images as $image) {
//            $category->addMedia($request->image)->toMediaCollection();
//        }
        return $this->handleResponse($category, 'Category Created');
    }

    public function createListing(Request $request)
    {
        $request->request->add(['userId' => $request->user()->id]);
        $request->validate([
//            'filters' => 'required',
//            'filters.*' => 'required|string',
            'categoryId' => 'required|integer|exists:categories,id',
            'userId' => 'required|integer|exists:users,id',
            'name' => 'required|string',
            'whatsappNumber' => 'required|integer|digits:10',
            'phoneNumber' => 'required|integer|digits:10',
            'location' => 'required|string',
            'price' => 'required|integer',
            'unit' => 'required|string',
            'description' => 'required|string',
            'type' => 'required|string',
            'images' => 'required',
        ],
            [
                'categoryId.required' => 'Category Id is required',
                'userId.required' => 'UserId is required',
                'name.required' => 'Name is required',
                'whatsappNumber.required' => 'Whatsapp Number is required',
                'whatsappNumber.integer' => 'Whatsapp Number should be numbers only',
                'whatsappNumber.digits' => 'Whatsapp Number should have 10 digits',
                'phoneNumber.required' => 'Phone Number is required',
                'phoneNumber.integer' => 'Phone Number should be numbers only',
                'phoneNumber.digits' => 'Phone Number should have 10 digits',

                'location.required' => 'Location is required',
                'price.required' => 'Price is required',
                'price.integer' => 'Price should be integer',
                'unit.required' => 'Unit is Required',
                'description.required' => 'Description is required',
                'type.required' => 'Type is required',
                'images.required' => 'Images are required',
            ]
        );
        if ($request->type == 'broiler') {
            $request->validate([
                'averageWeight' => 'required',
                'age' => 'required|integer',
                'availableQuantity' => 'required|integer',
                'bookingDate' => 'required'
            ],
                [
                    'averageWeight.required' => 'Average Weight is required',
                    'age.required' => 'Age is required',
                    'availableQuantity.required' => 'Available Quantity is required',
                    'bookingDate.required' => 'Booking Date is required'
                ]);
        } elseif ($request->type == 'chicks') {
            $request->validate([
                'breed' => 'required|string',
                'availableQuantity' => 'required|integer',
                'hatchingDate' => 'required'
            ],
                [
                    'breed.required' => 'Breed is required',
                    'availableQuantity.required' => 'AvailableQuantity is required',
                    'availableQuantity.integer' => 'AvailableQuantity must be a integer',
                    'hatchingDate.required' => 'HatchingDate is required',
                    // 'hatchingDate.date' => 'HatchingDate should be a date',
                ]);
        } elseif ($request->type == 'hatchingEggs') {
            $request->validate([
                'breed' => 'required|string',
                'weight' => 'required|integer',
                'loadingDate' => 'required',
                'quantity' => 'required|integer',
                'hatchingGuarantee' => 'required|integer',
            ],
                [
                    'breed.required' => 'Breed is required',
                    'weight.required' => 'Weight is required',
                    'weight.integer' => 'Weight must be a integer',
                    'loadingDate.required' => 'LoadingDate is required',
                    'quantity.required' => 'Quantity is required',
                    'quantity.integer' => 'Quantity must be a integer',
                    'hatchingGuarantee.required' => 'Hatching guarantee is required',
                    'hatchingGuarantee.integer' => 'Hatching guarantee must be a integer',
                ]);
        } elseif ($request->type == 'eggs') {
            $request->validate([
                'eggType' => 'required|string',
                'eggTrayWeight' => 'required|integer',
                'loadingDate' => 'required|date',
                'quantity' => 'required|integer',
            ],
                [
                    'eggType.required' => 'eggType is required',
                    'eggTrayWeight.required' => 'eddTrayWeight is required',
                    'eggTrayWeight.integer' => 'eggTrayWeight must be an integer',
                    'loadingDate.required' => 'loadingDate is required',
                    'quantity.required' => 'quantity is required',
                    'quantity.integer' => 'quantity must be a integer',
                ]);
        } elseif ($request->type == 'poultryEquipment') {
            $request->validate([
                'typeOfEquipment' => 'required|string',
            ],
                [
                    'typeOfEquipment.required' => 'typeOfEquipment is Required',
                ]);
        } elseif ($request->type == 'labService') {
            $request->validate([
                'typeOfFacility' => 'required|string',
            ], [
                'typeOfFacility.required' => 'typeOfFacility is required',
            ]);
        } elseif ($request->type == 'feed') {
            $request->validate([
                'brandName' => 'required|string',
                'preStarter' => 'required|string',
                'starter' => 'required|string',
                'finisher' => 'required|string',
                'concentrate' => 'required|string',
            ], [
                'brandName.required' => 'brandName is required',
                'preStarter.required' => 'preStarter is required',
                'starter.required' => 'starter is required',
                'finisher.required' => 'finisher is required',
                'concentrate.required' => 'concentrate is required',
            ]);
        } elseif ($request->type == 'rawMaterial') {
            $request->validate([
                'supplierOf' => 'required|string',
            ],
                [
                    'supplierOf.required' => 'supplierOf is required',
                ]);
        } elseif ($request->type == 'medicine') {
            $request->validate([
                'brandName' => 'required|string',
            ],
                [
                    'brandName.required' => 'brandName is required',
                ]);
        }

        DB::beginTransaction();
        try {
            $listing = Listing::create($request->all());
            isset($request->filters) ? $listing->attachTags($request->filters) : null;
            foreach ($request->images as $image) {
                $listing->addMedia($image)->toMediaCollection();
            }
            if ($request->type == 'broiler') {
                $listing->extra_attributes = [
                    'averageWeight' => $request->averageWeight,
                    'age' => $request->age,
                    'availableQuantity' => $request->availableQuantity,
                    'bookingDate' => $request->bookingDate,
                ];
                $listing->save();
            } elseif ($request->type == 'chicks') {
                $listing->extra_attributes = [
                    'breed' => $request->breed,
                    'availableQuantity' => $request->availableQuantity,
                    'loadingDate' => $request->loadingDate,
                ];
                $listing->save();
            } elseif ($request->type == 'hatchingEggs') {
                $listing->extra_attributes = [
                    'breed' => $request->breed,
                    'weight' => $request->weight,
                    'loadingDate' => $request->loadingDate,
                    'quantity' => $request->quantity,
                    'hatchingGuarantee' => $request->hatchingGuarantee,

                ];
                $listing->save();
            } elseif ($request->type == 'eggs') {
                $listing->extra_attributes = [
                    'eggType' => $request->eggType,
                    'eggTrayWeight' => $request->eggTrayWeight,
                    'loadingDate' => $request->loadingDate,
                    'quantity' => $request->quantity,
                ];
                $listing->save();
            } elseif ($request->type == 'poultryEquipment') {
                $listing->extra_attributes = [
                    'typeOfEquipment' => $request->typeOfEquipment,
                ];
                $listing->save();
            } elseif ($request->type == 'labService') {
                $listing->extra_attributes = [
                    'typeOfFacility' => $request->typeOfFacility,
                ];
                $listing->save();
            } elseif ($request->type == 'feed') {
                $listing->extra_attributes = [
                    'brandName' => $request->brandName,
                    'preStarter' => $request->preStarter,
                    'starter' => $request->starter,
                    'finisher' => $request->finisher,
                    'concentrate' => $request->concentrate,
                ];
                $listing->save();
            } elseif ($request->type == 'rawMaterial') {
                $listing->extra_attributes = [
                    'supplierOf' => $request->supplierOf,
                ];
                $listing->save();
            } elseif ($request->type == 'medicine') {
                $listing->extra_attributes = [
                    'brandName' => $request->brandName,
                ];
                $listing->save();
            }

            if(isset($request->lat) && isset($request->long)){
                $listing->extra_attributes['lat'] = $request->lat;
                $listing->extra_attributes['long'] = $request->long;
                $listing->save();
            }
            DB::commit();
            return $this->handleResponse($listing, 'Listing Created');

        } catch (\Exception $e) {
            DB::rollback();
            return $this->handleError($e, 'Unable to Create Listing');
        }
    }

    public function editListing(Request $request)
    {
        $request->request->add(['userId' => $request->user()->id]);

        $request->validate([
            'listingId' => 'required|exists:listings,id',
            'categoryId' => 'required|integer|exists:categories,id',
            'userId' => 'required|integer|exists:users,id',
            'name' => 'required|string',
            'whatsappNumber' => 'required|integer|digits:10',
            'phoneNumber' => 'required|integer|digits:10',
            'location' => 'required|string',
            'price' => 'required|integer',
            'unit' => 'required|string',
            'description' => 'required|string',
            'type' => 'required|string',
            'images' => 'required',
        ], [
            'categoryId.required' => 'Category Id is required',
            'userId.required' => 'UserId is required',
            'name.required' => 'Name is required',
            'whatsappNumber.required' => 'Whatsapp Number is required',
            'whatsappNumber.integer' => 'Whatsapp Number should be numbers only',
            'whatsappNumber.digits' => 'Whatsapp Number should have 10 digits',
            'phoneNumber.required' => 'Phone Number is required',
            'phoneNumber.integer' => 'Phone Number should be numbers only',
            'phoneNumber.digits' => 'Phone Number should have 10 digits',

            'location.required' => 'Location is required',
            'price.required' => 'Price is required',
            'price.integer' => 'Price should be integer',
            'unit.required' => 'Unit is Required',
            'description.required' => 'Description is required',
            'type.required' => 'Type is required',
            'images.required' => 'Images are required',
        ]);
        if ($request->type == 'broiler') {
            $request->validate([
                'averageWeight' => 'required',
                'age' => 'required|integer',
                'availableQuantity' => 'required|integer',
                'bookingDate' => 'required'
            ],
                [
                    'averageWeight.required' => 'Average Weight is required',
                    'age.required' => 'Age is required',
                    'availableQuantity.required' => 'Available Quantity is required',
                    'bookingDate.required' => 'Booking Date is required'
                ]);
        } elseif ($request->type == 'chicks') {
            $request->validate([
                'breed' => 'required|string',
                'availableQuantity' => 'required|integer',
                'hatchingDate' => 'required'
            ],
                [
                    'breed.required' => 'Breed is required',
                    'availableQuantity.required' => 'AvailableQuantity is required',
                    'availableQuantity.integer' => 'AvailableQuantity must be a integer',
                    'hatchingDate.required' => 'HatchingDate is required',
                    // 'hatchingDate.date' => 'HatchingDate should be a date',
                ]);
        } elseif ($request->type == 'hatchingEggs') {
            $request->validate([
                'breed' => 'required|string',
                'weight' => 'required|integer',
                'loadingDate' => 'required',
                'quantity' => 'required|integer',
                'hatchingGuarantee' => 'required|integer',
            ],
                [
                    'breed.required' => 'Breed is required',
                    'weight.required' => 'Weight is required',
                    'weight.integer' => 'Weight must be a integer',
                    'loadingDate.required' => 'LoadingDate is required',
                    'quantity.required' => 'Quantity is required',
                    'quantity.integer' => 'Quantity must be a integer',
                    'hatchingGuarantee.required' => 'Hatching guarantee is required',
                    'hatchingGuarantee.integer' => 'Hatching guarantee must be a integer',
                ]);
        } elseif ($request->type == 'eggs') {
            $request->validate([
                'eggType' => 'required|string',
                'eggTrayWeight' => 'required|integer',
                'loadingDate' => 'required|date',
                'quantity' => 'required|integer',
            ],
                [
                    'eggType.required' => 'eggType is required',
                    'eggTrayWeight.required' => 'eddTrayWeight is required',
                    'eggTrayWeight.integer' => 'eggTrayWeight must be an integer',
                    'loadingDate.required' => 'loadingDate is required',
                    'quantity.required' => 'quantity is required',
                    'quantity.integer' => 'quantity must be a integer',
                ]);
        } elseif ($request->type == 'poultryEquipment') {
            $request->validate([
                'typeOfEquipment' => 'required|string',
            ],
                [
                    'typeOfEquipment.required' => 'typeOfEquipment is Required',
                ]);
        } elseif ($request->type == 'labService') {
            $request->validate([
                'typeOfFacility' => 'required|string',
            ], [
                'typeOfFacility.required' => 'typeOfFacility is required',
            ]);
        } elseif ($request->type == 'feed') {
            $request->validate([
                'brandName' => 'required|string',
            ], [
                'brandName.required' => 'brandName is required',
            ]);
        } elseif ($request->type == 'rawMaterial') {
            $request->validate([
                'supplierOf' => 'required|string',
            ],
                [
                    'supplierOf.required' => 'supplierOf is required',
                ]);
        } elseif ($request->type == 'medicine') {
            $request->validate([
                'brandName' => 'required|string',
            ],
                [
                    'brandName.required' => 'brandName is required',
                ]);
        }

        $listing = Listing::find($request->listingId);
        if ($listing->userId == auth()->user()->id) {
            DB::beginTransaction();
            try {
                $listing->update($request->all());
                isset($request->filters) ? $listing->syncTags($request->filters) : null;
                foreach ($request->images as $image) {
                    $listing->addMedia($image)->toMediaCollection();
                }
                if ($request->type == 'broiler') {
                    $listing->extra_attributes = [
                        'averageWeight' => $request->averageWeight,
                        'age' => $request->age,
                        'availableQuantity' => $request->availableQuantity,
                        'bookingDate' => $request->bookingDate,
                    ];
                    $listing->save();
                } elseif ($request->type == 'chicks') {
                    $listing->extra_attributes = [
                        'breed' => $request->breed,
                        'availableQuantity' => $request->availableQuantity,
                        'loadingDate' => $request->loadingDate,
                    ];
                    $listing->save();
                } elseif ($request->type == 'hatchingEggs') {
                    $listing->extra_attributes = [
                        'breed' => $request->breed,
                        'weight' => $request->weight,
                        'loadingDate' => $request->loadingDate,
                        'quantity' => $request->quantity,
                        'hatchingGuarantee' => $request->hatchingGuarantee,

                    ];
                    $listing->save();
                } elseif ($request->type == 'eggs') {
                    $listing->extra_attributes = [
                        'eggType' => $request->eggType,
                        'eggTrayWeight' => $request->eggTrayWeight,
                        'loadingDate' => $request->loadingDate,
                        'quantity' => $request->quantity,
                    ];
                    $listing->save();
                } elseif ($request->type == 'poultryEquipment') {
                    $listing->extra_attributes = [
                        'typeOfEquipment' => $request->typeOfEquipment,
                    ];
                    $listing->save();
                } elseif ($request->type == 'labService') {
                    $listing->extra_attributes = [
                        'typeOfFacility' => $request->typeOfFacility,
                    ];
                    $listing->save();
                } elseif ($request->type == 'feed') {
                    $listing->extra_attributes = [
                        'brandName' => $request->brandName,
                    ];
                    $listing->save();
                } elseif ($request->type == 'rawMaterial') {
                    $listing->extra_attributes = [
                        'supplierOf' => $request->supplierOf,
                    ];
                    $listing->save();
                } elseif ($request->type == 'medicine') {
                    $listing->extra_attributes = [
                        'brandName' => $request->brandName,
                    ];
                    $listing->save();
                }
                DB::commit();
                return $this->handleResponse($listing, 'Listing Created');

            } catch (\Exception $e) {
                DB::rollback();
                return $this->handleError($e, 'Unable to Create Listing');
            }
        } else {
            return $this->handleError(null, 'You can only edit listings created by you.');
        }
    }

    public function filters(Request $request)
    {


        return $this->handleResponse($this->getCategoryFilters($request->categoryId), 'Filters');
    }

    public function getCategoryFilters($categoryId)
    {
        $filter = collect();
        $c = Categories::find($categoryId);
        $filters = $c->tags;
        foreach ($filters as $f) {
            $filter->push($f->name);
        }
//
//        Listing::where('categoryId', $categoryId)->with('tags')->get()->map(function ($listing) use ($filter) {
//            $filter->push($listing->tags->pluck('name'));
//            return $listing->tags;
//        });

        return Arr::flatten($filter);
    }
}
