<?php

namespace App\Http\Controllers;

use App\Models\Jobs;
use Illuminate\Http\Request;
use App\Http\Controllers\API\ResponseController as ResponseController;
use Illuminate\Support\Facades\DB;

class JobsController extends ResponseController
{
    public function applyJob(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'experience' => 'required',
            'phone' => 'required|digits:10',
            'applyingForPost' => 'required|string',
            'cv' => 'required|file|max:5120|mimes:pdf,docx,doc'
        ],
        [
            'name.required'=>'name is required',
            'experience.required'=>'experience is required',
            'phone.required'=>'phone is required',
            'phone.digits'=>'phone should be 10 digits',
            'applyingForPost.required'=>'applyingForPost is required',
            'cv.required'=>'CV is required',
            'cv.mimes'=>'CV should be a PDF or Word(docx/doc) file under 5 MB',
        ]);
        DB::begintransaction();
        try {
            $job = new Jobs();
            $job->userId = $request->user()->id;
            $job->name = $request->name;
            $job->experience = $request->experience;
            $job->contactNumber = $request->phone;
            $job->applyingForPost = $request->applyingForPost;
            $job->save();
            $job->addMedia($request->cv)->toMediaCollection();
            DB::commit();
            return $this->handleResponse(null, 'Applied for Job Successfully');
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->handleError($e, 'Unable to apply for job');
        }
    }
}
