<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function index()
    {
        return Categories::all();
    }

    public function createCategory(Request $request)
    {
        $category = Categories::create([
            'name' => $request->categoryName,
            'description' => $request->categoryDescription,
        ]);
        $category
            ->addMedia($request->categoryImage)
            ->toMediaCollection();
        return $category;
    }
}
