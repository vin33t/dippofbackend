<?php

namespace App\Http\Controllers\API;

use App\Models\TempUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\API\ResponseController as ResponseController;
use App\Models\User;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;


class AuthController extends ResponseController
{

    public function login(Request $request)
    {
        if (Auth::attempt(['phone' => $request->phone, 'password' => 'password'])) {
            $auth = Auth::user();
            $success['token'] = $auth->createToken('LaravelSanctumAuth')->plainTextToken;
//            $success['name'] =  $auth->name;

            return $this->handleResponse($success, 'User logged-in!');
        } else {
            return $this->handleError('Unauthorised.', ['error' => 'Unauthorised']);
        }
    }

    function generateRandomString($length = 8)
    {
        $characters = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


    public function register(Request $request, $referralCode)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|integer|digits:10|unique:users,phone',
        ]);

        if ($validator->fails()) {
            return $this->handleError($validator->errors());
        }


        $user = User::create([
            'phone' => $request->phone,
            'password' => bcrypt('password'),
            'referralCode' => $referralCode
        ]);
        $success['token'] = $user->createToken('LaravelSanctumAuth')->plainTextToken;

        return $this->handleResponse($success, 'User successfully registered!');
    }

    public function sendOtp(Request $request)
    {
        // $otp = rand(10000, 99999);
        $otp = 12345;
        $tUser = TempUser::where('phone', $request->phone);
        if ($tUser->count()) {
            $tUser->first()->update([
                'otp' => $otp
            ]);
        } else {
            $tuser = TempUser::updateOrCreate([
                'phone' => $request->phone,
                'otp' => $otp
            ]);
        }

        // send otp
        return $this->handleResponse(['phone' => $request->phone], 'OTP Sent Successfully');

    }

    public function verifyOtp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|exists:temp_users,phone|digits:10',
            'otp' => 'required|digits:5'
        ],
            [
//                'phone.exists' => 'Phone Number'
            ]
        );
        if ($validator->fails()) {
            return $this->handleError($validator->errors());
        }
        $codeValidated = 0;
        while (!$codeValidated) {
            $referralCode = $this->generateRandomString();
            if (!User::where('referralCode', $referralCode)->count()) {
                $codeValidated = 1;
            }
        }
        $tUser = TempUser::where('phone', $request->phone)->first();
        if ($tUser->otp == $request->otp) {
            $tUser->delete();
            if (User::where('phone', $request->phone)->count()) {
                return $this->login($request);
            } else {
                return $this->register($request, $referralCode);
            }
        } else {
            return $this->handleError("", null);
        }
    }


}
