@extends('adminlte::page')

@section('title', 'Listings')

@section('content_header')
    <h1>Listings</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h4>{{ $listings->count() }} Listing</h4>
        </div>
        <div class="card-body">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Category</th>
                    <th scope="col">Number</th>
                    <th scope="col">Location</th>
                    <th scope="col">Price</th>
                    <th scope="col">Images</th>
                </tr>
                </thead>
                <tbody>
                @foreach($listings as $listing)
                    <tr>
                        <th scope="row">{{ $loop->index + 1 }}</th>
                        <td>{{ $listing->name }}</td>
                        <td>{{ $listing->category->name }}</td>
                        <td>Whatsapp: {{ $listing->whatsappNumber }} <br> Phone: {{ $listing->phoneNumber }}</td>
                        <td>{{ $listing->location }}</td>
                        <td>{{ $listing->price }}</td>
                        <td>@if($listing->images->count())
                                @foreach($listing->images as $image)
                                    <img src="{{ $image }}" alt="" width="80">
                                @endforeach
                            @else
                                No images Found
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
