@extends('adminlte::page')

@section('title', 'Banners')

@section('content_header')
    <h1>Banners</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h4>{{ $banners->count() }} Banners
                <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><i
                        class="fa fa-plus"></i></button>
            </h4>

            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Add New Banner</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <form action="{{ route('banner.upload') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Name</label>
                                        <input type="text" name="bannerName" class="form-control"
                                               placeholder="Banner Name" required>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Redirects To</label>
                                        <select name="redirectsTo" class="form-control" required>
                                            <option value="">--Select Category--</option>
                                            @foreach(\App\Models\Categories::all() as $category)
                                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Banner Image</label>
                                        <input type="file" name="bannerImage" class="form-control" placeholder="Banner Image"
                                               accept="image/png, image/gif, image/jpeg" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-success">Add</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Redirects To</th>
                    <th scope="col">Image</th>
                    <th scope="col">status</th>
                </tr>
                </thead>
                <tbody>
                @forelse($banners as $banner)
                    <tr>
                        <td>{{ $loop->index + 1 }}</td>
                        <td>{{ $banner->name }}</td>
                        <td>{{ $banner->category->name }}</td>
                        <td><img src="{{ $banner->getFirstMediaUrl() }}" alt="{{ $banner->name }} Image" width="220"></td>
                        <td>{{ $banner->status ? 'Active' : 'Disabled'}}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5">No Banners Available</td>
                    </tr>
                @endforelse
                </tbody>
            </table>

        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
