@extends('adminlte::page')

@section('title', 'Doctors')

@section('content_header')
    <h1>Doctors</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h4>{{ $doctors->count() }} Doctors</h4>
        </div>
        <div class="card-body">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Whatsapp Number</th>
                    <th scope="col">Location</th>
                    <th scope="col">Degree</th>
                    <th scope="col">Experience</th>
                    <th scope="col">Specialization</th>
                    <th scope="col">Fee</th>
                    <th scope="col">Status</th>
                </tr>
                </thead>
                <tbody>
                @forelse($doctors as $doctor)
                    <tr>
                        <th scope="row">{{ $loop->index + 1 }}</th>
                        <td>{{ $doctor->name }}</td>
                        <td>{{ $doctor->whatsapp_no }}</td>
                        <td>{{ $doctor->location }}</td>
                        <td>{{ $doctor->degree }}</td>
                        <td>{{ $doctor->experience }}</td>
                        <td>{{ $doctor->specialization }}</td>
                        <td>{{ $doctor->fees }}</td>
                        <td>{{ $doctor->approved ? 'Approved' : 'Pending For Approval' }}
                            @if(!$doctor->approved)
                                <form action="{{ route('doctor.statusUpdate',['doctor'=>$doctor]) }}" method="POST">
                                    @csrf
                                <select name="approveDoctor" id="approveDoctor">
                                    <option value="">--Select Status--</option>
                                    <option value="approve">Approve</option>
                                </select>
                                    <button class="btn btn-primary btn-sm" type="submit">Save</button>
                                </form>
                                @else
                                <form action="{{ route('doctor.statusUpdate',['doctor'=>$doctor]) }}" method="POST">
                                    @csrf
                                    <select name="approveDoctor" id="approveDoctor">
                                        <option value="">--Select Status--</option>
                                        <option value="cancel">Cancel Approval</option>
                                    </select>
                                    <button class="btn btn-primary btn-sm" type="submit">Save</button>
                                </form>
                            @endif
                        </td>
                    </tr>
                @empty
                    <tr><td colspan="4">No Records Available</td></tr>
                @endforelse
                </tbody>
            </table>

        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
